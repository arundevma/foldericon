﻿Imports System
Imports System.IO
Imports System.Text

Public Class Form1
    Dim txtcolor As String
    Dim colorselected As Boolean = False


    Dim filestr As Byte()

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        fldr.ShowDialog()
        If fldr.SelectedPath = "" Then
            Exit Sub
        End If
        txtselected.Text = fldr.SelectedPath
        If txtselected.Text.Length = 3 Then
            txtselected.AppendText("desktop.ini")
        Else
            txtselected.AppendText("\desktop.ini")
        End If

    End Sub





    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        file1.CheckPathExists = True
        file1.ShowDialog()
        If file1.FileName = "" Then
            Exit Sub
        End If



        picbx.BackgroundImage = Image.FromFile(file1.FileName)



    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If txtselected.Text = "" Then
            MsgBox("Select folder")
            Exit Sub
        End If
        If colorselected = False Then
            MsgBox("Select text colour")
            colorselected = False
            Exit Sub
        End If
        Dim fs As FileStream
        If File.Exists(txtselected.Text) = True Then
            My.Computer.FileSystem.DeleteFile(txtselected.Text)
        End If
        fs = File.Create(txtselected.Text)
        filestr = New UTF8Encoding(True).GetBytes("[ExtShellFolderViews]" & ControlChars.NewLine)
        fs.Write(filestr, 0, filestr.Length)
        filestr = New UTF8Encoding(True).GetBytes("{BE098140-A513-11D0-A3A4-00C04FD706EC}={BE098140-A513-11D0-A3A4-00C04FD706EC}" & ControlChars.NewLine)
        fs.Write(filestr, 0, filestr.Length)
        filestr = New UTF8Encoding(True).GetBytes("[{BE098140-A513-11D0-A3A4-00C04FD706EC}]" & ControlChars.NewLine)
        fs.Write(filestr, 0, filestr.Length)
        filestr = New UTF8Encoding(True).GetBytes("Attributes=1" & ControlChars.NewLine)
        fs.Write(filestr, 0, filestr.Length)
        filestr = New UTF8Encoding(True).GetBytes("IconArea_Image=" & file1.FileName & ControlChars.NewLine)
        fs.Write(filestr, 0, filestr.Length)
        filestr = New UTF8Encoding(True).GetBytes("IconArea_Text=" & txtcolor & ControlChars.NewLine)
        fs.Write(filestr, 0, filestr.Length)
        filestr = New UTF8Encoding(True).GetBytes("[.ShellClassInfo]" & ControlChars.NewLine)
        fs.Write(filestr, 0, filestr.Length)
        filestr = New UTF8Encoding(True).GetBytes("ConfirmFileOp=0" & ControlChars.NewLine)
        fs.Write(filestr, 0, filestr.Length)
        fs.Close()
        txtselected.Text = "Done...."
        picbx.BackgroundImage = Nothing

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Select Case ComboBox1.SelectedIndex
            Case 0
                txtcolor = "0x0000FFFF"
                colorselected = True
            Case 1
                txtcolor = "0x00FFFFFF"
                colorselected = True
            Case 2
                txtcolor = "0x00000000"
                colorselected = True
            Case 3
                txtcolor = "0x000080FF"
                colorselected = True
            Case 4
                txtcolor = "0x00008000"
                colorselected = True
            Case 5
                txtcolor = "0x000000FF"
                colorselected = True
        End Select
    End Sub
End Class
